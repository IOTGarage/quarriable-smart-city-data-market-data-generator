''' This code generates random data in json format for NGSI DataModel - Museum, 
convert them to RDF graph and load them into a triple store database'''

from pyngsi.ngsi import DataModel
import sys
import random
from random import uniform 
import datetime
import json
from faker import Faker
from geojson import Point
import string
from rdflib import Graph, Literal, RDF, URIRef, Namespace 
from rdflib.namespace import FOAF , XSD, SSN, SOSA 
import urllib.parse
import pandas as pd 
import stardog


# A function to generate synthetic data
def model_generator():
	
	# Generate random unique id
	def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
		return ''.join(random.choice(chars) for _ in range(size))


	model = DataModel(id = id_generator(), type='Museum')
	model.add_url('dataProvider', 'https://fiware-datamodels.readthedocs.io/en/latest/PointOfInterest/Museum/doc/spec/index.html')
	
	# Source: https://www.alva.org.uk/details.cfm?p=610
	Name = ['British Museum','Tate Modern', 'National Gallery', 'NHM (South Kensington', 'Southbank Centre', 'V&A South Kensington', 'Science Museum', 'Tower of London', 'Royal Museums Greenwich', 'Somerset House']
	model.add('name', random.choice(Name))

	# Source : https://thesaurus.yourdictionary.com/museum
	AlternateName = ['Gallery','Collection', 'Institution', 'Library', 'Depository', 'Repository', 
	'Treasury', 'Menagerie', 'Zoological-garden', 'Botanical-garden', 'Herbarium', 'Hall', 'Vault', 'Place of Exhibition', 'Archive']
	model.add('alternateName', random.choice(AlternateName))


	OpeningHoursSpecification = [{'dayOfWeek': 'Mo, Wed, Thu, Fr','closes' : '19:30','opens' : '11:00'},
	{'dayOfWeek': 'Sat','closes' : '21:00','opens' : '10:00'}, {'dayOfWeek': 'Sun', 'closes' : '15:00','opens' : '10:00'}]
	model.add('openingHoursSpecification', OpeningHoursSpecification)


	Description= [' According to Sanahl et al. [1], The museum is a non-profit, permanent institution in the service of society and its development, open to the public, which acquires, conserves, researches, communicates and exhibits the tangible and intangible heritage of humanity and its environment for the purposes of education, study and enjoyment. ']
	model.add('description', Description)

	Source = ['https://www.alva.org.uk/details.cfm?p=610']
	model.add('source', Source)

	
	# https://en.wikipedia.org/wiki/Periods_in_Western_art_history
	ArtPeriod = ['Ancient Classical art', 'Medieval art', 'Renaissance','Renaissance to Neoclassicism', 'Romanticism', 'Romanticism to modern art',
	'Modern art', 'Contemporary art']
	model.add('artPeriod', random.choice(ArtPeriod))
		
	
	#https://fiware-datamodels.readthedocs.io/en/latest/PointOfInterest/Museum/doc/spec/index.html
	MuseumType = ['appliedArts', 'scienceAndTechnology','fineArts', 'music', 'history', 'sacredArt', 'archaeology', 'specials', 'decorativeArts', 'literature',
	'medicineAndPharmacy', 'maritime','transports', 'military', 'wax', 'popularArtsAndTraditions', 'numismatic', 'unesco','ceramics', ' sumptuaryArts',
	'naturalScience', 'prehistoric', 'ethnology', 'railway','mining', ' textile', 'sculpture', 'multiDisciplinar', 'painting', 'paleonthology', 
	'modernArt', 'thematic', ' architecture', 'museumHouse', 'cathedralMuseum', 'diocesanMuseum', 'universitary', 'contemporaryArt', 'bullfighting']
	model.add('museumType', random.choice(MuseumType))

	
	#https://fiware-datamodels.readthedocs.io/en/latest/PointOfInterest/Museum/doc/spec/index.html
	Facilities = ['elevator', 'cafeteria', 'shop', 'auditory', ' conferenceRoom', 'audioguide', 'cloakRoom','forDisabled', 
	'forBabies', 'guidedTour', 'restaurant', 'ramp', 'reservation']
	model.add('facilities', random.choice(Facilities))


	# Generate British Museum location
	# Location = Point((51.5194, -0.1270))
	Location = Point((uniform(-50, 50), uniform(0.1,-0.2)))
	model.add('location', Location)
	

	Address = [{'adressCountry': Faker().country(),'addressLocality' : Faker().city(),'streetAddress' : Faker().address()}]
	model.add('address', Address)


	return model 

mydata = [model_generator() for i in range(100)]
# Save output into a json file.
with open('museum100.json', 'w') as outfile:
	json.dump(mydata , outfile, indent=2)
	# print(mydata)

# Read in json file 
df = pd.read_json(r'museum100k.json')

# Set up graph model
g = Graph()
udeo = Namespace('http://www.w3id.org/udeo/')
ngsi = Namespace('https://uri.etsi.org/ngsi-ld/')
geojson = Namespace('https://purl.org/geojson/vocab#')
context = Namespace('https://uri.etsi.org/ngsi-ld/default-context/')
fiware = Namespace('https://uri.fiware.org/ns/data-models#')
schema = Namespace('https://schema.org/')
uri = URIRef('https://schema.lab.fiware.org/ld/context.jsonld/')
xsd = Namespace('http://www.w3.org/2001/XMLSchema#')
sosa = Namespace('http://www.w3.org/ns/sosa/')

# Convert data to graph model 
for index, row in df.iterrows():

	g.add((URIRef(udeo+row['id']), RDF.type, sosa.Observation)) 

	g.add((URIRef(udeo+row['id']), sosa.hasFeatureOfInterest, fiware.Museum)) 
		
	g.add((URIRef(udeo+row['id']), ngsi.location, Literal(row['location'])))

	g.add((URIRef(udeo+row['id']), ngsi.description, Literal(row['description'])))

	g.add((URIRef(udeo+row['id']), ngsi.name, Literal(row['name'])))

	g.add((URIRef(udeo+row['id']), URIRef(schema+'address'), Literal(row['address'])))

	g.add((URIRef(udeo+row['id']), URIRef(schema+'alternateName'), Literal(row['alternateName'])))

	g.add((URIRef(udeo+row['id']), fiware.artPeriod ,Literal(row['artPeriod'])))

	g.add((URIRef(udeo+row['id']), fiware.facilities , Literal(row['facilities'])))

	g.add((URIRef(udeo+row['id']), fiware.museumType , Literal(row['museumType'])))

	g.add((URIRef(udeo+row['id']), fiware.openingHoursSpecification , Literal(row['openingHoursSpecification'])))

	g.add((URIRef(udeo+row['id']), fiware.dataProvider, Literal(row['dataProvider'])))

	g.add((URIRef(udeo+row['id']), fiware.source, Literal(row['source'])))

# Serialize graph into turtle format
g.serialize(format='turtle').decode('UTF-8')

# Save datamodel to disk
g.serialize('museum100.rdf', format='ttl')

# Push data model to triple store.
conn_details = {
  'endpoint': 'http://localhost:5820',
  'username': 'admin',
  'password': 'admin'
}
with stardog.Admin(**conn_details) as admin:
    museum100 = admin.new_database('museum100')

conn = stardog.Connection('museum100', **conn_details)

conn.begin()

conn.add(
    stardog.content.File('museum100.rdf', stardog.content_types.TURTLE),

)

conn.commit()
