''' This code generates random data in json format for NGSI DataModel - ParkingSpot, 
convert them to RDF graph and load them into a triple store database'''

from pyngsi.ngsi import DataModel
import sys
import random
from random import uniform 
import datetime
import json
from faker import Faker
from geojson import Point
import string
from rdflib import Graph, Literal, RDF, URIRef, Namespace 
from rdflib.namespace import FOAF , XSD, SSN, SOSA 
import urllib.parse
import pandas as pd 
import stardog

# A function to generate synthetic data
def model_generator():
	
	# Generate random unique id
	def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
		return ''.join(random.choice(chars) for _ in range(size))

	model = DataModel(id = id_generator(), type='ParkingSpot')
	
	model.add_url('dataProvider', 'https://fiware-datamodels.readthedocs.io/en/latest/Parking/ParkingSpot/doc/spec/index.html')

	Status = ['occupied', 'free', 'closed', 'unknown']
	
	DateTime =str( datetime.datetime.now() )
    
	model.add('status', random.choice(Status), metadata ={'timestamp':{'type' : 'DateTime','value' : DateTime}})
	
	Category = ['onstreet', 'offstreet']
	model.add('category', random.choice(Category))

	model.add_relationship('refParkingSite', 'https://www.johnlewis.com','offstreet')
	
	#https://www.etsi.org/deliver/etsi_gs/CIM/001_099/009/01.01.01_60/gs_CIM009v010101p.pdf
	Name = ['A-01','A-02','A-03','A-04','A-05','A-06','A-07','A-08','A-09','A-10','A-11','A-12','A-13']
	model.add('name', random.choice(Name))

	# Generate air quality stations locations
	# Location = Point(( 478068, -174430))
	Location = Point((uniform(-50, 50), uniform(0.1,-0.2)))
	model.add('location', Location)

	return model 

mydata = [model_generator() for i in range(100)]
# Save output into a json file.
with open('parkingSpot100.json', 'w') as outfile:
	json.dump(mydata , outfile, indent=2)
	# print(mydata)

# Read in json file 
df = pd.read_json(r'parkingSpot100.json')

# Set up graph model
g = Graph()
udeo = Namespace('http://www.w3id.org/udeo/')
ngsi = Namespace('https://uri.etsi.org/ngsi-ld/')
geojson = Namespace('https://purl.org/geojson/vocab#')
context = Namespace('https://uri.etsi.org/ngsi-ld/default-context/')
fiware = Namespace('https://uri.fiware.org/ns/data-models#')
schema = Namespace('https://schema.org/')
uri = URIRef('https://schema.lab.fiware.org/ld/context.jsonld/')
xsd = Namespace('http://www.w3.org/2001/XMLSchema#')
sosa = Namespace('http://www.w3.org/ns/sosa/')

# Convert data to graph model 
for index, row in df.iterrows():

	g.add((URIRef(ID+row['id']), RDF.type, sosa.Observation)) 

	g.add((URIRef(ID+row['id']), sosa.hasFeatureOfInterest, fiware.ParkingSpot)) 

	g.add((URIRef(ID+row['id']), ngsi.location, Literal(row['location'])))

	g.add((URIRef(ID+row['id']), ngsi.status, Literal(row['status'])))

	g.add((URIRef(ID+row['id']), URIRef(schema+'name'), Literal(row['name'])))

	g.add((URIRef(ID+row['id']), fiware.category,Literal(row['category'])))

	g.add((URIRef(ID+row['id']), fiware.refParkingSite, Literal(row['refParkingSite'])))

	g.add((URIRef(ID+row['id']), fiware.dataProvider, Literal(row['dataProvider'])))

# Serialize graph into turtle format
g.serialize(format='turtle')

# Save datamodel to disk
g.serialize('parkingSpot100.rdf', format='ttl')

# Push data model to triple store.
conn_details = {
  'endpoint': 'http://localhost:5820',
  'username': 'admin',
  'password': 'admin'
}
with stardog.Admin(**conn_details) as admin:
    parkingSpot100= admin.new_database('parkingSpot100')

conn = stardog.Connection('parkingSpot100', **conn_details)

conn.begin()

conn.add(stardog.content.File('parkingSpot100.rdf', stardog.content_types.TURTLE),)

conn.commit()
