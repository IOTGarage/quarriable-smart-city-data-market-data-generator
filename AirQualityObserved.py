''' This code generates random data in json format for NGSI DataModel - AirQualityObserved, 
convert them to RDF graph and load them into a triple store database'''

from pyngsi.ngsi import DataModel
import sys
import random
from random import uniform 
import datetime
import json
from faker import Faker
from geojson import Point
import string
from rdflib import Graph, Literal, RDF, URIRef, Namespace 
from rdflib.namespace import FOAF , XSD, SSN, SOSA 
import urllib.parse
import pandas as pd 
import stardog

# A function to generate synthetic data
def model_generator():
	
	# Generate random unique id
	def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
		return ''.join(random.choice(chars) for _ in range(size))

	model = DataModel(id = id_generator(), type='AirQualityObserved')
	model.add_url('dataProvider', 'https://fiware-datamodels.readthedocs.io/en/latest/Environment/AirQualityObserved/doc/spec/index.html')
	
	# Datetime = datetime.datetime.now()
	model.add('dateObserved', datetime.datetime.now())	
	
	# USA EPA Agency https://www.airnow.gov/aqi/aqi-basics/ ['good', 'moderate', 'unhealthyForSensitiveGroups', 'unhealthy', 'veryUnhealthy', 'hazardous'] 
	'''Generate air quality level according to UK standards,  UK levels
	https://uk-air.defra.gov.uk/assets/documents/reports/cat14/
	1210261047_Individuals_interpretation_of_air_quality_information_customer_insight_&_awareness_study.pdf'''
	AirQualityLevel = ['Low', 'Moderate', 'High', 'Very', 'High']
	model.add('airQualityLevel', random.choice(AirQualityLevel))
	model.add('CO_Level', random.choice(AirQualityLevel))

	# Generate air quality index numerical ranges, https://aqicn.org/city/london/
	AirQualityIndex = random.randint(0,174)  
	model.add('airQualityIndex', AirQualityIndex)
	
	# Generate air quality index values and their corresponding codes. http://wiki.goodrelations-vocabulary.org/Documentation/UN/CEFACT_Common_Codes
	CodeList = ['C81','C25','B97','DD','D61','D62','A91','A91','M43','M44','D27','H57','MTR','E96','H27','GP','GQ'] 
	unit = {'value': random.choice(CodeList)}
	model.add('CO', random.randint(1,999), metadata = {'unitCode': {'value': random.choice(CodeList)}})
	model.add('NO', random.randint(1,999), metadata = {'unitCode': {'value': random.choice(CodeList)}})
	model.add('SO2', random.randint(1,999), metadata = {'unitCode':{'value': random.choice(CodeList)}})
	model.add('NOx', random.randint(1,999), metadata = {'unitCode': {'value': random.choice(CodeList)}})
	model.add('NO2', random.randint(1,999), metadata = {'unitCode': {'value': random.choice(CodeList)}})	
	
	'''Generate air quality station, #https://uk-air.defra.gov.uk/
	networks/find-sites.php?group_id=4&postcode=CF10&radius=50000&latitude=51.474000&longitude
	=-3.176000&action=results&view=location&search=Search+Network'''
	airqualitystation = 'https://uk-air.defra.gov.uk/data/so/GB_Station_GB0580A?format=application/xml' 
	model.add_relationship('refPointOfInterest', airqualitystation, id_generator())

	# Generate wind direction values.
	WindDirection = random.randint(0,360)
	model.add('windDirection', WindDirection)
	

	# Generate air quality stations locations
	Location = Point((uniform(-50, 50), uniform(0.1,-0.2)))
	model.add('location', Location)
	
	
	Address = [{'adressCountry': Faker().country(),'addressLocality' : Faker().city(),'streetAddress' : Faker().address()}]
	model.add('address', Address)

	# Generate reliability ratio floats rounded up to the 2nd decimal place.
	Reliability =  round(random.random(),2)
	model.add('reliability', Reliability)

	# Generate Relative Humidity floats, rounded up to the 2nd decimal place. 
	RelativeHumidity = round(random.random(),2)
	model.add('reliability', RelativeHumidity)

	'''Generate precepitation numerical air quality index values.  # https://uk-air.defra.gov.uk
	/assets/documents/reports/cat14/
	1210261047_Individuals_interpretation_of_air_quality_information_customer_insight_&_awareness_study.pdf'''
	Precipitation = random.randint(1,500) 
	model.add('precipitation', Precipitation)

	return model 

mydata = [model_generator() for i in range(100)]
# Save output into a json file.
with open('air100.json', 'w') as outfile:
	json.dump(mydata , outfile, indent=2)
	# print(mydata)

# Read in json file 
df = pd.read_json(r'air100.json')

# Set up graph model
g = Graph()
udeo = Namespace('http://www.w3id.org/udeo/')
ngsi = Namespace('https://uri.etsi.org/ngsi-ld/')
geojson = Namespace('https://purl.org/geojson/vocab#')
context = Namespace('https://uri.etsi.org/ngsi-ld/default-context/')
fiware = Namespace('https://uri.fiware.org/ns/data-models#')
schema = Namespace('https://schema.org/')
uri = URIRef('https://schema.lab.fiware.org/ld/context.jsonld/')
xsd = Namespace('http://www.w3.org/2001/XMLSchema#')
sosa = Namespace('http://www.w3.org/ns/sosa/')

# Convert data to graph model 
for index, row in df.iterrows():

	g.add((URIRef(udeo+row['id']), RDF.type, sosa.Observation)) 

	g.add((URIRef(udeo+row['id']), sosa.observedProperty, fiware.AirQualityObserved)) 

	g.add((URIRef(udeo+row['id']), ngsi.location, Literal(row['location'])))

	g.add((URIRef(udeo+row['id']), URIRef(schema +'address'), Literal(row['address'])))

	g.add((URIRef(udeo+row['id']), fiware.airQualityIndex, Literal(row['airQualityIndex'])))

	g.add((URIRef(udeo+row['id']), fiware.airQualityLevel, Literal(row['airQualityLevel'])))

	g.add((URIRef(udeo+row['id']), fiware.precipitation, Literal(row['precipitation'])))

	g.add((URIRef(udeo+row['id']), fiware.reliability, Literal(row['reliability'])))

	g.add((URIRef(udeo+row['id']), fiware.windDirection , Literal(row['windDirection'])))

	g.add((URIRef(udeo+row['id']), fiware.dateObserved, Literal(row['dateObserved'])))

	g.add((URIRef(udeo+row['id']), fiware.dataProvider, Literal(row['dataProvider'])))

	g.add((URIRef(udeo+row['id']), context.CO, Literal(row['CO'])))

	g.add((URIRef(udeo+row['id']), context.CO_Level, Literal(row['CO_Level'])))

	g.add((URIRef(udeo+row['id']), context.NO, Literal(row['NO'])))

	g.add((URIRef(udeo+row['id']), context.NO2, Literal(row['NO2'])))

	g.add((URIRef(udeo+row['id']), context.NOx, Literal(row['NOx'])))

	g.add((URIRef(udeo+row['id']), context.SO2, Literal(row['SO2'])))

# Serialize graph into turtle format
g.serialize(format='turtle').decode('UTF-8')

# Save datamodel to disk
g.serialize('air100.rdf', format='ttl')

# Push data model to triple store.
conn_details = {
  'endpoint': 'http://localhost:5820',
  'username': 'admin',
  'password': 'admin'
}
with stardog.Admin(**conn_details) as admin:
    air100k = admin.new_database('air100')

conn = stardog.Connection('air100', **conn_details)

conn.begin()

conn.add(
    stardog.content.File('air100.rdf', stardog.content_types.TURTLE),
)

conn.commit()
