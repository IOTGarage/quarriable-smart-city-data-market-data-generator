''' This code generates random data in json format for NGSI DataModel - BikeHireDockingStation, 
convert them to RDF graph and load them into a triple store database'''

from pyngsi.ngsi import DataModel
import sys
import random
from random import uniform 
import datetime
import json
from faker import Faker
from geojson import Point
import string
from rdflib import Graph, Literal, RDF, URIRef, Namespace 
from rdflib.namespace import FOAF , XSD, SSN, SOSA 
import urllib.parse
import pandas as pd 
import stardog

# A function to generate synthetic data
def model_generator():
	
	#Generate random unique id
	def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
		return ''.join(random.choice(chars) for _ in range(size))

	#Buid DataModel by specifying id and type.
	model = DataModel(id = id_generator(), type='BikeHireDockingStation')
	
	#Add dataProvider url
	model.add_url('dataProvider', 'https://fiware-datamodels.readthedocs.io/en/latest/Transportation/Bike/BikeHireDockingStation/doc/spec/index.html#data-model')

	#Add random choice of status 
	Status = ['occupied', 'free', 'closed', 'unknown']
	DateTime = datetime.datetime.now()
	model.add('status', random.choice(Status), metadata = {'timestamp':{'type' : 'DateTime','value' : 'DateTime'}})

	#Add random available bike number.
	AvailableBikeNumber = random.randint(0,50)
	model.add('availableBikeNumber', AvailableBikeNumber)

	#Add random free slot number
	FreeSlotNumber = random.randint(0,50)
	model.add('freeSlotNumber', FreeSlotNumber)

	#Add ranom location
	Location = Point((uniform(-50, 50), uniform(0.1,-0.2)))
	model.add('location', Location)

	#Add random Address 
	Address = [{'adressCountry': Faker().country(),'addressLocality' : Faker().city(),'streetAddress' : Faker().address()}]
	model.add('address', Address)
	
	return model 

# Generate entities as desired by adjusting the figure in range(). e.g. range(100) will generate 100 distinct DataModels.
# Generate 100+ different DataModels 
mydata = [model_generator() for i in range(100)]

# Save output into a json file.
with open('BikeHireDockingStation100.json', 'w') as outfile:
	json.dump(mydata , outfile, indent=2)
	# print(mydata)

# read in json file 
df = pd.read_json(r'BikeHireDockingStation100.json')

#set up graph model
g = Graph()
udeo = Namespace('http://www.w3id.org/udeo/')
ngsi = Namespace('https://uri.etsi.org/ngsi-ld/')
geojson = Namespace('https://purl.org/geojson/vocab#')
context = Namespace('https://uri.etsi.org/ngsi-ld/default-context/')
fiware = Namespace('https://uri.fiware.org/ns/data-models#')
schema = Namespace('https://schema.org/')
uri = URIRef('https://schema.lab.fiware.org/ld/context.jsonld/')
xsd = Namespace('http://www.w3.org/2001/XMLSchema#')
sosa = Namespace('http://www.w3.org/ns/sosa/')


#convert data to graph model 
for index, row in df.iterrows():

	g.add((URIRef(udeo+row['id']), RDF.type, sosa.Observation)) 

	g.add((URIRef(udeo+row['id']), sosa.hasFeatureOfInterest, fiware.BikeHireDockingStation))

	g.add((URIRef(udeo+row['id']), ngsi.location, Literal(row['location'])))

	g.add((URIRef(udeo+row['id']), ngsi.status, Literal(row['status'])))

	g.add((URIRef(udeo+row['id']), URIRef(schema+'address'), Literal(row['address'])))

	g.add((URIRef(udeo+row['id']), fiware.availableBikeNumber,Literal(row['availableBikeNumber'])))

	g.add((URIRef(udeo+row['id']), fiware.freeSlotNumber, Literal(row['freeSlotNumber'])))

	g.add((URIRef(udeo+row['id']), fiware.dataProvider, Literal(row['dataProvider'])))

    # serialize graph into turtle format
    g.serialize(format='turtle').decode('UTF-8')

    #save datamodel to disk
    g.serialize('BikeHireDockingStation100.rdf', format='ttl')

# Push data model to triple store.
conn_details = {
  'endpoint': 'http://localhost:5820',
  'username': 'admin',
  'password': 'admin'
}
with stardog.Admin(**conn_details) as admin:
    BikeHireDockingStation100= admin.new_database('BikeHireDockingStation100')

conn = stardog.Connection('BikeHireDockingStation100', **conn_details)

conn.begin()

conn.add(
    stardog.content.File('BikeHireDockingStation100.rdf', stardog.content_types.TURTLE),

)

conn.commit()
