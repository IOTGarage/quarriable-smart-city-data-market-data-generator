''' This code generates random data in json format for NGSI DataModel - Beach, 
convert them to RDF graph and load them into a triple store database'''

from pyngsi.ngsi import DataModel
import sys
import random
from random import uniform 
import datetime
import json
from faker import Faker
from geojson import Point
import string
from rdflib import Graph, Literal, RDF, URIRef, Namespace 
from rdflib.namespace import FOAF , XSD, SSN, SOSA 
import urllib.parse
import pandas as pd 
import stardog

# A function to generate synthetic data
def model_generator():
	
	#Generate random unique id
	def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
		return ''.join(random.choice(chars) for _ in range(size))


	model = DataModel(id = id_generator(), type='Beach')
	
	
	model.add_url('dataProvider', 'https://fiware-datamodels.readthedocs.io/en/latest/PointOfInterest/Beach/doc/spec/index.html#normalized-example')
	
	
	Description = ['The part adjacent to shores']
	model.add('description', Description)


	Width = round(random.random(),2)
	model.add('width', Width)

	Length = round(random.random(),2)
	model.add('length', Length)



	AccessType = ['privateVehicle', 'onFoot', 'publicTransport']
	model.add('accessType', random.choice(AccessType))


	Location = Point((uniform(-50, 50), uniform(0.1,-0.2)))
	model.add('location', Location)


	Facilities = ['promenade', 'showers', 'cleaningServices', 'lifeGuard','sunshadeRental', 'sunLoungerRental', 'waterCraftRental','toilets', 'touristOffice',
	'litterBins','telephone','surfPracticeArea','accessforDisabled']
	model.add('facilities', random.choice(Facilities))


	Source = ['https://https://www.thebeachguide.co.uk/']
	model.add('source', Source)

	# Source: https://www.roughguides.com/gallery/the-best-british-beaches/
	Name = ['Pelistry Bay', 'Isles of Scilly','Crosby Beach', 'Hunmanby Gap', 'Scolt Head', 'Compton Bay' , ' Bamburgh Beach', 
	'Rhossili Beach', 'Beadnell Beach', 'Bigbury-on-Sea', 'Blackpool Pleasure Beach', 'Barafundle Bay', 'Longsands, Tynemouth', 'Sandwood Bay',
	'Somerset House', 'West Wittering', 'Whitesands Bay', 'Studland Bay', 'Kynance Cove', 'Portstewart Strand','Achnahaird']
	model.add('name', random.choice(Name))

	Address = [{'adressCountry': Faker().country(),'addressLocality' : Faker().city(),'streetAddress' : Faker().address()}]
	model.add('address', Address)


	BeachType = ['whiteSand', 'urban', 'isolated', 'calmWaters', 'blueFlag', 'Q-Quality', 'strongWaves', 'windy', 'blackSand']
	model.add('beachType', random.choice(BeachType))


	OccupationRate = ['Low',' Medium', 'High']
	model.add('occupationRate', random.choice(OccupationRate))


	return model 

mydata = [model_generator() for i in range(100)]
# Save output into a json file.
with open('beach100.json', 'w') as outfile:
	json.dump(mydata , outfile, indent=2)
	# print(mydata)

# Read in json file 
df = pd.read_json(r'beach100.json')

# Set up graph model
g = Graph()
udeo = Namespace('http://www.w3id.org/udeo/')
ngsi = Namespace('https://uri.etsi.org/ngsi-ld/')
geojson = Namespace('https://purl.org/geojson/vocab#')
context = Namespace('https://uri.etsi.org/ngsi-ld/default-context/')
fiware = Namespace('https://uri.fiware.org/ns/data-models#')
schema = Namespace('https://schema.org/')
uri = URIRef('https://schema.lab.fiware.org/ld/context.jsonld/')
xsd = Namespace('http://www.w3.org/2001/XMLSchema#')
sosa = Namespace('http://www.w3.org/ns/sosa/')


# Convert data to graph model 
for index, row in df.iterrows():
    
    g.add((URIRef(udeo+row['id']), RDF.type, sosa.Observation)) 

    g.add((URIRef(udeo+row['id']), sosa.hasFeatureOfInterest, fiware.Beach)) 

    g.add((URIRef(udeo+row['id']), ngsi.location, Literal(row['location'])))
  
    g.add((URIRef(udeo+row['id']), ngsi.description, Literal(row['description'])))

    g.add((URIRef(udeo+row['id']), ngsi.name, Literal(row['name'])))

    g.add((URIRef(udeo+row['id']), URIRef(schema+'address'), Literal(row['address'])))
   
    g.add((URIRef(udeo+row['id']), fiware.accessType,Literal(row['accessType'])))
  
    g.add((URIRef(udeo+row['id']), fiware.beachType, Literal(row['beachType'])))
  
    g.add((URIRef(udeo+row['id']), fiware.facilities , Literal(row['facilities'])))
  
    g.add((URIRef(udeo+row['id']), fiware.dataProvider, Literal(row['dataProvider'])))

    g.add((URIRef(udeo+row['id']), fiware.length, Literal(row['length'])))

    g.add((URIRef(udeo+row['id']), fiware.width, Literal(row['width'])))

    g.add((URIRef(udeo+row['id']), fiware.source, Literal(row['source'])))

    g.add((URIRef(udeo+row['id']), fiware.occupationRate, Literal(row['occupationRate'])))


# Serialize graph into turtle format
g.serialize(format='turtle').decode('UTF-8')

# Save datamodel to disk
g.serialize('beach100.rdf', format='ttl')

# Push data model to triple store.
conn_details = {
  'endpoint': 'http://localhost:5820',
  'username': 'admin',
  'password': 'admin'
}
with stardog.Admin(**conn_details) as admin:
    beach100 = admin.new_database('beach100')

conn = stardog.Connection('beach100', **conn_details)

conn.begin()

conn.add(
    stardog.content.File('beach100.rdf', stardog.content_types.TURTLE),

)

conn.commit()
