''' This code generates random data in json format for NGSI DataModel - NoiseLevelObserved, 
convert them to RDF graph and load them into a triple store database'''

from pyngsi.ngsi import DataModel
import sys
import random
from random import uniform 
import datetime
import json
from faker import Faker
from geojson import Point
import string
from rdflib import Graph, Literal, RDF, URIRef, Namespace 
from rdflib.namespace import FOAF , XSD, SSN, SOSA 
import urllib.parse
import pandas as pd 
import stardog


def model_generator():
	
	#Generate random unique id
	def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
		return ''.join(random.choice(chars) for _ in range(size))

	model = DataModel(id = id_generator(), type='NoiseLevelObserved')
	
	
	model.add_url('dataProvider', 'https://fiware-datamodels.readthedocs.io/en/latest/Environment/NoiseLevelObserved/doc/spec/index.html#noise-level-observed')
	
	# Generate noise random locations
	Location = Point((uniform(-50, 50), uniform(0.1,-0.2)))
	model.add('location',Location,'geo:json')


	DatetimeFrom = datetime.datetime.now()
	model.add('dateObservedFrom', DatetimeFrom)	

	DatetimeTo= datetime.datetime.now()
	model.add('dateObservedTo', DatetimeTo)	


	LAEQ = 'A-weighted, equivalent, sound level'
	model.add('LAeq', round(random.random(),2), metadata = {'description':{'value': LAEQ}})


	LAMAX = 'A-weighted, equivalent, sound level'
	model.add('LAmax', round(random.random(),2), metadata = {'description':{'value': LAMAX}})

	Las = 'A-weighted, equivalent, sound level'
	model.add('LAS', round(random.random(),2), metadata = {'description': {'value': Las}})

	LAEQ_d = 'A-weighted, equivalent, sound level'
	model.add('LAeq_d', round(random.random(),2), metadata = {'description': {'value' : LAEQ_d}})
           

	Frequencies = [['250', 40], ['315',40], ['400', 40], ['500',40],['630',40],['800',40],['1000',40],['1250',40], ['1600',40],['2000',40],['2500',40],['3150',40], ['4000', 40],['8000',40],['10000',40]]

	model.add('frequencies', Frequencies, metadata = {'description': {'value':'A-weighted, frequency, sound level'}})

	return model 

# Generate 100 observations, the number of observations can be adjusted from the range below. 
mydata = [model_generator() for i in range(100)] 

# Save output into a json file.
with open('noise100.json', 'w') as outfile:
	json.dump(mydata , outfile, indent=2)
	# print(mydata)

# Read in json file 
df = pd.read_json(r'noise100.json')

# Set up graph model
g = Graph()
udeo = Namespace('http://www.w3id.org/udeo/')
ngsi = Namespace('https://uri.etsi.org/ngsi-ld/')
geojson = Namespace('https://purl.org/geojson/vocab#')
context = Namespace('https://uri.etsi.org/ngsi-ld/default-context/')
fiware = Namespace('https://uri.fiware.org/ns/data-models#')
schema = Namespace('https://schema.org/')
uri = URIRef('https://schema.lab.fiware.org/ld/context.jsonld/')
xsd = Namespace('http://www.w3.org/2001/XMLSchema#')
sosa = Namespace('http://www.w3.org/ns/sosa/')


# Convert data to graph model 
for index, row in df.iterrows():

	g.add((URIRef(ID+row['id']), RDF.type, sosa.Observation)) 

	g.add((URIRef(ID+row['id']), sosa.hasFeatureOfInterest, fiware.NoiseLevelObserved))
		
	g.add((URIRef(ID+row['id']), ngsi.location, Literal(row['location'])))

	g.add((URIRef(ID+row['id']), fiware.dateObservedFrom, Literal(row['dateObservedFrom'])))

	g.add((URIRef(ID+row['id']), fiware.dateObservedTo, Literal(row['dateObservedTo'])))

	g.add((URIRef(ID+row['id']), fiware.frequencies, Literal(row['frequencies'])))

	g.add((URIRef(ID+row['id']), fiware.dataProvider, Literal(row['dataProvider'])))

	g.add((URIRef(ID+row['id']), context.LAS, Literal(row['LAS'])))

	g.add((URIRef(ID+row['id']), context.LAeq, Literal(row['LAeq'])))

	g.add((URIRef(ID+row['id']), context.LAeq_d, Literal(row['LAeq_d'])))

	g.add((URIRef(ID+row['id']), context.LAmax, Literal(row['LAmax'])))

    # Serialize graph into turtle format
    g.serialize(format='turtle').decode('UTF-8')

    # Save datamodel to disk
    g.serialize('noise100.rdf', format='ttl')

# Push data model to triple store.
conn_details = {
  'endpoint': 'http://localhost:5820',
  'username': 'admin',
  'password': 'admin'
}
with stardog.Admin(**conn_details) as admin:

    noise100 = admin.new_database('noise100')

conn = stardog.Connection('noise100', **conn_details)

conn.begin()

conn.add(
    stardog.content.File('noise100.rdf', stardog.content_types.TURTLE),)
conn.commit()
