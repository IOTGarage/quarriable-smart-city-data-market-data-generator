# Query Interface for Smart City Internet of Things Data Marketplaces

These codes generate synthetic data observations in JSON, convert them to graph models (Turtle format) and load them into triple store databases.

**Description:**
Data models follow the FIWARE reference data context model (NGSI v2) and label the entities used in smart applications

1.	AirQuaityObserved:
This data model describes an air quality observation in a specific time and location.
https://github.com/smart-data-models/dataModel.Environment/blob/master/AirQualityObserved/README.md

2.  NoiseLevelObserved:
This data model describes an acoustic parameters observation that quantifies the pressure of noise level in a specific time and location.
https://github.com/smart-data-models/dataModel.Environment/blob/master/NoiseLevelObserved/README.md

3. BikeHireDockingStation:
This data model describes bikes to hire features, location, and availability. 
https://github.com/smart-data-models/dataModel.Transportation/blob/master/BikeHireDockingStation/README.md.

4. ParkingSpot:
This data model describes a particular area where a vehicle can park.  
https://github.com/smart-data-models/dataModel.Parking/blob/master/ParkingSpot/README.md

5. Beach:
This data model describes a beach profile, including location, type, and occupancy rate. This data model describes a beach profile.   
https://github.com/smart-data-models/dataModel.PointOfInterest/blob/master/Beach/README.md
        

6. Museum:
This data model describes a museum profile, including type and location.
https://github.com/smart-data-models/dataModel.PointOfInterest/blob/master/Museum/README.md



****Requirements

- Python 3.8+

**** packages:**

Faker, pyngsi, geojson, rdflib, pandas and stardog




